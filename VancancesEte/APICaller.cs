﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace VancancesEte
{
    class APICaller
    {
        public async static Task<string> GetJSONAsync(string request)
        {
            string res = "";
            HttpWebRequest wReq = WebRequest.Create(request) as HttpWebRequest;
            wReq.ContentType = "application/json";
            wReq.Method = "GET";
            wReq.ContentLength = 0;

            try
            {
                using(HttpWebResponse wRes = await wReq.GetResponseAsync() as HttpWebResponse)
                {
                    using(StreamReader reader = new StreamReader(wRes.GetResponseStream()))
                    {
                        res = reader.ReadToEnd();
                        reader.Close();
                    }
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return res;
        }

        public static async Task<byte[]> GetDataAsync(string request)
        {
            HttpWebRequest wReq = WebRequest.Create(request) as HttpWebRequest;
            try
            {
                using (HttpWebResponse wRes = await wReq.GetResponseAsync() as HttpWebResponse)
                {
                    using (BinaryReader reader = new BinaryReader(wRes.GetResponseStream()))
                    {
                        using(MemoryStream ms = new MemoryStream())
                        {
                            int readSize = 1024 * 4;
                            int lastRead = 0;
                            do
                            {
                                byte[] buff = new byte[readSize];
                                lastRead = reader.Read(buff, 0, readSize);
                                await ms.WriteAsync(buff, 0, lastRead);
                            } while (lastRead != readSize && lastRead > 0);
                            return ms.ToArray();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return null;
        }
    }
}
