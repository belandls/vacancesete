﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace VancancesEte.Converters
{
    class DateToRangeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime? d = (DateTime?)value;
            if (d != null)
            {
                int off = (int)parameter;
                DateTime final = d.Value.AddDays(off);
                return final.ToString("dd MMM") + String.Format(" - {0}", (char)(65 + off));
            }
            else
            {
                return "";
            }
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
