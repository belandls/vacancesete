﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VancancesEte.Models;

namespace VancancesEte
{
    class DBManager
    {
        private const string DB_NAME = "artists.sqlite";

        private string _conn_str;

        public DBManager()
        {
            _conn_str = String.Format("Data Source={0};Version=3", DB_NAME);
        }

        private async Task<bool> CreateDBIfAbsent()
        {
            if (!File.Exists(DB_NAME))
            {
                try
                {
                    SQLiteConnection.CreateFile(DB_NAME);

                    using (SQLiteConnection conn = new SQLiteConnection(_conn_str))
                    {
                        string sql;
                        SQLiteCommand cmd;
                        conn.Open();

                        sql = "CREATE TABLE Artist (ID integer primary key autoincrement, Name nvarchar(512), SongkickID INTEGER)";
                        cmd = new SQLiteCommand(sql, conn);

                        if (await cmd.ExecuteNonQueryAsync() == 0)
                        {
                            return false;
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "CreateDBIfAbsent");
                }

            }
            return true;
        }

        public async Task<bool> SaveArtistsToDB(IEnumerable<LocalArtist> artists)
        {
            if (!await CreateDBIfAbsent())
            {
                return false;
            }

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(_conn_str))
                {
                    StringBuilder insertSql = new StringBuilder();
                    SQLiteCommand deleteCmd, insertCmd;
                    conn.Open();

                    deleteCmd = new SQLiteCommand("DELETE FROM Artist", conn);

                    await deleteCmd.ExecuteNonQueryAsync();

                    insertSql.Append("BEGIN TRANSACTION;");
                    for (int i = 0; i < artists.Count(); i++)
                    {
                        insertSql.AppendFormat("INSERT INTO Artist(Name, SongkickID) VALUES (@{0}, @S{0});", i);
                    }
                    insertSql.Append("COMMIT;");

                    insertCmd = new SQLiteCommand(insertSql.ToString(), conn);

                    for (int i = 0; i < artists.Count(); i++)
                    {
                        insertCmd.Parameters.Add(new SQLiteParameter(String.Format("@{0}", i), artists.ElementAt(i).Name));
                        insertCmd.Parameters.Add(new SQLiteParameter(String.Format("@S{0}", i), artists.ElementAt(i).SongkickID));
                    }

                    if (await insertCmd.ExecuteNonQueryAsync() == 0)
                    {
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "SaveArtistsToDB");
            }
            return true;

        }

        public async Task<List<LocalArtist>> GetArtistsFromDB()
        {
            List<LocalArtist> artists = new List<LocalArtist>();
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(_conn_str))
                {
                    conn.Open();
                    SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM Artist", conn);
                    var rdr = await cmd.ExecuteReaderAsync();

                    while (rdr.Read())
                    {
                        LocalArtist temp = new LocalArtist((string)rdr["Name"], (long)rdr["SongkickID"]);
                        artists.Add(temp);
                    }

                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "GetArtistsFromDB");
            }

            return artists;
        }

    }
}
