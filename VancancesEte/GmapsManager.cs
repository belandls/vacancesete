﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using VancancesEte.Models;

namespace VancancesEte
{
    class GmapsManager
    {
        private const string API_KEY = "AIzaSyA3usv4k7D6w5hsCs2aK4KRoGyE38L6Y-M";
        private const string API_ENDPOINT = "https://maps.googleapis.com/maps/api";

        public GmapsManager() { }

        public async Task<BitmapImage> GetMapFromMarkers(IEnumerable<Marker> markers, int width, int height)
        {
                StringBuilder sb = new StringBuilder();
                foreach(Marker m in markers)
                {
                    sb.Append(m.ToString()).Append("&");
                }
                if(sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }
                string req = String.Format("{0}/staticmap?size={1}x{2}&scale=2&{3}&key={4}", API_ENDPOINT, width, height, sb.ToString(), API_KEY);
                byte[] res = await APICaller.GetDataAsync(req);

                BitmapImage img = new BitmapImage();
                using (MemoryStream ms = new MemoryStream(res))
                {
                    img.BeginInit();
                    img.StreamSource = ms;
                    img.CacheOption = BitmapCacheOption.OnLoad;
                    img.EndInit();
                }
                return img;
        }


    }
}
