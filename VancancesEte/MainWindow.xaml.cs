﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;
using SpotifyAPI.Web.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VancancesEte.Converters;
using VancancesEte.Models;

namespace VancancesEte
{
    //AIzaSyA3usv4k7D6w5hsCs2aK4KRoGyE38L6Y-M
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private DBManager _db_man;
        private SongkickManager _sk_man;
        private GmapsManager _gm_man;
        private List<ObservableCollection<ArtistEvent>> _event_lists;

        public MainWindow()
        {
            InitializeComponent();
            _db_man = new DBManager();
            _sk_man = new SongkickManager();
            _gm_man = new GmapsManager();

            _event_lists = new List<ObservableCollection<ArtistEvent>>(14);
            Style s = (Style)FindResource("sCalendar");
            DateToRangeConverter conv = new DateToRangeConverter();
            for(int i = 0; i < 14; i++)
            {
                _event_lists.Add(new ObservableCollection<ArtistEvent>());
                ListBox lb = new ListBox();
                lb.Style = s;
                Grid.SetRow(lb, 1 + (i / 7) * 2);
                Grid.SetColumn(lb, i % 7);
                lb.ItemsSource = _event_lists[i];
                gCal.Children.Add(lb);
                
                TextBlock tb = new TextBlock();
                tb.HorizontalAlignment = HorizontalAlignment.Center;
                tb.VerticalAlignment = VerticalAlignment.Center;
                Binding bnd = new Binding("SelectedDate");
                bnd.Mode = BindingMode.OneWay;
                bnd.ElementName = "dtStart";
                bnd.Converter = conv;
                bnd.ConverterParameter = i;
                BindingOperations.SetBinding(tb, TextBlock.TextProperty, bnd);
                Grid.SetRow(tb, (i / 7) * 2);
                Grid.SetColumn(tb, i % 7);
                gCal.Children.Add(tb);
            }

        }

        private async void cmdFillFromSpotify_Click(object sender, RoutedEventArgs e)
        {
            await FillArtistsFromSpotifyAsync();
        }

        private async Task FillArtistsFromSpotifyAsync()
        {
            cpr.IsActive = true;
            cpr.IsIndeterminate = true;
            cpr.Status = "Gathering artists from Spotify";
            List<LocalArtist> artists = await GetArtistsFromSpotify();//Task.Run(() => GetArtistsFromSpotify());
            ObservableCollection<LocalArtist> uiArtists = new ObservableCollection<LocalArtist>(artists.OrderBy((a) => a.Name));
            lstArtists.ItemsSource = uiArtists;

            cpr.IsActive = false;
            cpr.IsIndeterminate = false;
            cpr.Status = "";
        }

        private async Task<List<LocalArtist>> GetArtistsFromSpotify()
        {
            List<LocalArtist> artists = new List<LocalArtist>();
            HashSet<string> artistCache = new HashSet<string>();

            WebAPIFactory webApiFactory = new WebAPIFactory("http://localhost", 8000, "8731f1c3b6ab4646a7c8b9a8c047bec8", SpotifyAPI.Web.Enums.Scope.UserLibraryRead, TimeSpan.FromSeconds(20));

            try
            {
                List<SavedTrack> totalListOfTracks;
                using (SpotifyWebAPI spotify = await webApiFactory.GetWebApi())
                {
                    int currentOffset = 0;
                    int limit = 50;
                    Paging<SavedTrack> userTracks = await spotify.GetSavedTracksAsync(limit, currentOffset);
                    totalListOfTracks = new List<SavedTrack>(userTracks.Items);
                    while (userTracks.HasNextPage())
                    {
                        currentOffset += limit;
                        userTracks = await spotify.GetSavedTracksAsync(limit, currentOffset);
                        userTracks.Items.ForEach((t) => totalListOfTracks.Add(t));
                    }
                }

                if (totalListOfTracks != null)
                {
                    foreach (SavedTrack st in totalListOfTracks)
                    {
                        foreach (var artist in st.Track.Artists)
                        {
                            
                            if (!artistCache.Contains(artist.Name))
                            {
                                artistCache.Add(artist.Name);
                                artists.Add(new LocalArtist(artist.Name));
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return artists;
        }

        private void lstArtists_KeyDown(object sender, KeyEventArgs e)
        {
            if (lstArtists.SelectedItem != null && e.Key == Key.Delete)
            {
                ((ObservableCollection<LocalArtist>)lstArtists.ItemsSource).Remove((LocalArtist)lstArtists.SelectedItem);
            }
        }

        private async void cmdLoadFromDB_Click(object sender, RoutedEventArgs e)
        {
            cpr.IsActive = true;
            cpr.IsIndeterminate = true;
            cpr.Status = "Getting songkick IDs";

            var artists = await _db_man.GetArtistsFromDB();
            lstArtists.ItemsSource = new ObservableCollection<LocalArtist>(artists);
            //await _sk_man.GetSongkickIDFromArtistNameAsync((ObservableCollection<LocalArtist>)lstArtists.ItemsSource);

            //cpr.Status = "Saving updated artists to DB";

            //await _db_man.SaveArtistsToDB((ObservableCollection<LocalArtist>)lstArtists.ItemsSource);

            cpr.IsActive = false;
            cpr.IsIndeterminate = false;
            cpr.Status = "";
        }

        private async void cmdSaveToDB_Click(object sender, RoutedEventArgs e)
        {
            cpr.IsActive = true;
            cpr.IsIndeterminate = true;
            cpr.Status = "Saving current list to DB";
            var res = await _db_man.SaveArtistsToDB((ObservableCollection<LocalArtist>)lstArtists.ItemsSource).ConfigureAwait(false); //await Task.Run(() => SaveArtistsToDB((ObservableCollection<LocalArtist>)lstArtists.ItemsSource));
            if (res)
            {
                MessageBox.Show("Artists saved successfully!");
            }
            else
            {
                MessageBox.Show("Couldn't save the artists.");
            }

            cpr.IsActive = false;
            cpr.IsIndeterminate = false;
            cpr.Status = "";
        }

        private async void cmdGetArtistCalendars_Click(object sender, RoutedEventArgs e)
        {
            cpr.IsActive = true;
            cpr.IsIndeterminate = true;
            cpr.Status = "Getting calendars";

            var artists = (ObservableCollection<LocalArtist>)lstArtists.ItemsSource;

            await _sk_man.GetArtistsCalendarAsync(artists);

            lstArtistWithEvents.ItemsSource = artists.Where((a) => a.Events.Count > 0);

            cpr.IsActive = false;
            cpr.IsIndeterminate = false;
            cpr.Status = "";
        }

        private void cmdFillCalendar_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable<LocalArtist> artists = (IEnumerable < LocalArtist > )lstArtistWithEvents.ItemsSource;

            DateTime initialDate = dtStart.SelectedDate.Value;
            Dictionary<string, ObservableCollection<ArtistEvent>> lists = new Dictionary<string, ObservableCollection<ArtistEvent>>();
            for(int i = 0; i < 14; i++)
            {
                DateTime dateToCheck = initialDate.AddDays(i);
                lists.Add(dateToCheck.ToString("yyyy-MM-dd"), _event_lists[i]);
                _event_lists[i].Clear();
            }

            foreach(LocalArtist a in artists)
            {
                foreach(LocalEvent ev in a.Events)
                {
                    if (lists.ContainsKey(ev.Date))
                    {
                        lists[ev.Date].Add(new ArtistEvent(a, ev));
                    }
                }
            }

        }

        private async void cmdShowMaps_Click(object sender, RoutedEventArgs e)
        {
            List<Marker> markers = new List<Marker>();
            for(int i = 0; i < 14; i++)
            {
                ArtistEvent selected = _event_lists[i].FirstOrDefault((ae) => ae.IsSelected);
                if (selected != null)
                {
                    if (selected.Event.Type.StartsWith("F"))
                    {
                        markers.Add(new Marker(Colors.Yellow,(char)(65 + i), selected.Event.Longitude, selected.Event.Latitude));
                    }
                    else
                    {
                        markers.Add(new Marker((char)(65 + i), selected.Event.Longitude, selected.Event.Latitude));
                    }
                    
                }
            }
            var img = await _gm_man.GetMapFromMarkers(markers, 640, 640);
            var w = new ImgDisplay();
            w.imgMaps.Source = img;
            w.ShowDialog();
        }
    }
}
