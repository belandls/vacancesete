﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VancancesEte.Models
{
    class Artist
    {
        public string displayName { get; set; }
        public string uri { get; set; }
        public int id { get; set; }
        public List<object> identifier { get; set; }
        public string onTourUntil { get; set; }
    }
}
