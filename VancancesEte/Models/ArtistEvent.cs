﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VancancesEte.Models
{
    class ArtistEvent : ObservableObject
    {
        private LocalArtist _artist;
        private LocalEvent _event;
        private bool _is_selected;

        public LocalArtist Artist
        {
            get
            {
                return _artist;
            }
        }

        public LocalEvent Event
        {
            get
            {
                return _event;
            }
        }

        public bool IsSelected
        {
            get
            {
                return _is_selected;
            }
            set
            {
                _is_selected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        public ArtistEvent(LocalArtist a, LocalEvent e)
        {
            _artist = a;
            _event = e;
            _is_selected = false;
        }
    }
}
