﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VancancesEte.Models
{
    class Event
    {
        public string type { get; set; }
        public string status { get; set; }
        public double popularity { get; set; }
        public Start start { get; set; }
        public string displayName { get; set; }
        public object ageRestriction { get; set; }
        public Location location { get; set; }
        public string uri { get; set; }
        public int id { get; set; }
        public List<Performance> performance { get; set; }
        public Venue venue { get; set; }
    }
}
