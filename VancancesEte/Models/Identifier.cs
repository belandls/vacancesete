﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VancancesEte.Models
{
    class Identifier
    {
        public string href { get; set; }
        public string eventsHref { get; set; }
        public string mbid { get; set; }
        public string setlistsHref { get; set; }
    }
}
