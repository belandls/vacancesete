﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VancancesEte.Models
{
    class LocalArtist : ObservableObject
    {
        private long _songkick_id;
        private string _name;
        private List<LocalEvent> _events;

        public long SongkickID
        {
            get
            {
                return _songkick_id;
            }
            set
            {
                _songkick_id = value;
                OnPropertyChanged("SongkickID");
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public List<LocalEvent> Events
        {
            get
            {
                return _events;
            }
        }

        public LocalArtist(string name, long skID)
        {
            _name = name;
            _songkick_id = skID;
            _events = new List<LocalEvent>();
        }
        public LocalArtist(string name) : this(name,-1) { }
        public LocalArtist() : this("", -1) { }
    }
}
