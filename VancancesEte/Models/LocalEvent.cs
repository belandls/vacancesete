﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VancancesEte.Models
{
    class LocalEvent
    {
        private List<string> _other_artists;

        public string Name { get; set; }
        public string Time { get; set; }
        public string Date { get; set; }
        public string City { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Country { get; set; }
        public string VenueName { get; set; }
        public string Type { get; set; }
        public List<string> OtherArtists {
            get
            {
                return _other_artists;
            }
        }

        public LocalEvent()
        {
            _other_artists = new List<string>();
        }

    }
}
