﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VancancesEte.Models
{
    class Location
    {
        public string city { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
