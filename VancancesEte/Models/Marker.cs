﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Media;

namespace VancancesEte.Models
{
    class Marker
    {
        private Color? _couleur;
        private char _label;
        private double _long;
        private double _lat;

        public Marker(Color? c, char l, double lng, double lat)
        {
            _couleur = c;
            _label = char.ToUpper(l);
            _long = lng;
            _lat = lat;
        }

        public Marker(char l, double lng, double lat) : this(null, l, lng, lat) { }
        public Marker(double lng, double lat) : this(null, ' ', lng, lat) { }

        public override string ToString()
        {
            StringBuilder sb= new StringBuilder();
            sb.Append("markers=");
            
            if(_couleur != null)
            {
                sb.AppendFormat("color:0x{0:X2}{1:X2}{2:X2}%7C", _couleur.Value.R, _couleur.Value.G, _couleur.Value.B);
            }

            if (_label != ' ')
            {
                sb.Append("label:").Append(_label).Append("%7C");
            }

            sb.Append(_lat).Append(",").Append(_long);

            return sb.ToString();

        }
    }
}
