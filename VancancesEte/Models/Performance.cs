﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VancancesEte.Models
{
    class Performance
    {
        public Artist artist { get; set; }
        public string displayName { get; set; }
        public int billingIndex { get; set; }
        public string billing { get; set; }
        public int id { get; set; }
    }
}
