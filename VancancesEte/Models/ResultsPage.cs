﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VancancesEte.Models
{
    class ResultsPage<T>
    {
        public string status { get; set; }
        public Results<T> results { get; set; }
        public int perPage { get; set; }
        public int page { get; set; }
        public int totalEntries { get; set; }
    }
}
