﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VancancesEte.Models
{
    class Venue
    {
        public MetroArea metroArea { get; set; }
        public double lat { get; set; }
        public string displayName { get; set; }
        public double lng { get; set; }
        public string uri { get; set; }
        public int id { get; set; }
    }
}
