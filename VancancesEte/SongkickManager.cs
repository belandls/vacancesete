﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using VancancesEte.Models;

namespace VancancesEte
{
    class SongkickManager
    {
        private const string API_KEY = "u543oV7Qcx1SGX57";
        private const string API_ENDPOINT = "http://api.songkick.com/api/3.0";

        private HashSet<string> _accepted_countries;

        public SongkickManager() {
            _accepted_countries = new HashSet<string>();
            _accepted_countries.Add("US");
            _accepted_countries.Add("Canada");
            //JsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
        }

        public async Task<bool> GetArtistsCalendarAsync(IEnumerable<LocalArtist> artists)
        {
            List<Task<bool>> batch = new List<Task<bool>>();
            List<LocalArtist> artistsToProcess = new List<LocalArtist>();
            foreach(LocalArtist a in artists)
            {
                if(a.SongkickID != -1)
                {
                    artistsToProcess.Add(a);
                }
            }
            
            int parBatch = artistsToProcess.Count / 4;

            batch.Add(GetArtistCalendarBatchAsync(artistsToProcess, 0, parBatch));
            batch.Add(GetArtistCalendarBatchAsync(artistsToProcess, parBatch * 1, parBatch));
            batch.Add(GetArtistCalendarBatchAsync(artistsToProcess, parBatch * 2, parBatch));
            batch.Add(GetArtistCalendarBatchAsync(artistsToProcess, parBatch * 3, artistsToProcess.Count - (parBatch * 3)));

            await Task.WhenAll(batch.ToArray());

            return true;

        }

        private async Task<bool> GetArtistCalendarBatchAsync(IEnumerable<LocalArtist> artists, int start, int count)
        {
            
            int maxIndex = start + count;
            for (int i = start; i < maxIndex; i++)
            {
                LocalArtist aTemp = artists.ElementAt(i);
                string req = String.Format("{0}/artists/{1}/calendar.json?apikey={2}", API_ENDPOINT, aTemp.SongkickID, API_KEY);
                string res = await APICaller.GetJSONAsync(req);

                if (!string.IsNullOrEmpty(res))
                {
                    res = res.Replace("\"event\":", "\"entries\":");
                    RootObject<Event> deserialized = await Task.Run(() => JsonConvert.DeserializeObject<RootObject<Event>>(res,new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

                    if (deserialized.resultsPage.totalEntries > 0)
                    {
                        foreach(Event e in deserialized.resultsPage.results.entries)
                        {
                            if (_accepted_countries.Contains(e.venue.metroArea.country.displayName))
                            {
                                LocalEvent eTemp = new LocalEvent();
                                eTemp.City = e.location.city;
                                eTemp.Country = e.venue.metroArea.country.displayName;
                                eTemp.Date = e.start.date;
                                eTemp.Latitude = e.location.lat;
                                eTemp.Longitude = e.location.lng;
                                eTemp.Name = e.displayName;
                                eTemp.Time = e.start.time;
                                eTemp.Type = e.type;
                                eTemp.VenueName = e.venue.displayName;
                                
                                foreach(Performance p in e.performance)
                                {
                                    eTemp.OtherArtists.Add(p.displayName);
                                }
                                aTemp.Events.Add(eTemp);
                            }
                        }
                    }
                }

            }
            return true;
        }

        public async Task<bool> GetSongkickIDFromArtistNameAsync(IEnumerable<LocalArtist> artists)
        {
            int numOfArtists = artists.Count();
            int parBatch = numOfArtists / 4;
            List<Task<bool>> batch = new List<Task<bool>>();

            batch.Add(GetSonkickIdBatchAsync(artists, 0, parBatch));
            batch.Add(GetSonkickIdBatchAsync(artists, parBatch * 1, parBatch));
            batch.Add(GetSonkickIdBatchAsync(artists, parBatch * 2, parBatch));
            batch.Add(GetSonkickIdBatchAsync(artists, parBatch * 3, numOfArtists - (parBatch * 3)));

            await Task.WhenAll(batch.ToArray());

            return true;
        }

        private async Task<bool> GetSonkickIdBatchAsync(IEnumerable<LocalArtist> artists, int start, int count)
        {

            int maxIndex = start + count;
            for(int i = start; i < maxIndex; i++)
            {
                LocalArtist aTemp = artists.ElementAt(i);
                string req = BuildSingleSearchLink(aTemp.Name);
                string res = await APICaller.GetJSONAsync(req);

                if(!string.IsNullOrEmpty(res))
                {
                    res = res.Replace("\"artist\":", "\"entries\":");
                    RootObject<Artist> deserialized = await Task.Run(() => JsonConvert.DeserializeObject<RootObject<Artist>>(res));
                    
                    if (deserialized.resultsPage.totalEntries == 1)
                    {
                        aTemp.SongkickID = deserialized.resultsPage.results.entries[0].id;
                    }
                    else if(deserialized.resultsPage.totalEntries > 1) 
                    {
                        foreach(var r in deserialized.resultsPage.results.entries)
                        {
                            if(r.displayName.ToLower() == aTemp.Name.ToLower())
                            {
                                aTemp.SongkickID = r.id;
                                break;
                            }
                        }
                    }
                }

            }
            return true;
        }
        

        //http://api.songkick.com/api/3.0/search/artists.json?query=thank+you+scientist&apikey=u543oV7Qcx1SGX57
        private string BuildSingleSearchLink(string name)
        {
            string encodedName = HttpUtility.UrlEncode(name);
            return String.Format("{0}/search/artists.json?query={1}&apikey={2}", API_ENDPOINT, encodedName, API_KEY);
        }

        //http://api.songkick.com/api/3.0/artists/2931761/calendar.json?apikey=u543oV7Qcx1SGX57

    }
}
